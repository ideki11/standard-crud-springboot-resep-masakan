package com.example.resep.exceptions;

public class NotFoundException extends RuntimeException {    
        /**
	 * 
	 */
	private static final long serialVersionUID = -3374972414250960406L;

		public NotFoundException(String resourceName, String fieldName, Object fieldValue) {
            super(String.format("%s not found with %s : '%s'", resourceName, fieldName, fieldValue));

        }
}

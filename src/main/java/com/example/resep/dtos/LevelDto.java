package com.example.resep.dtos;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LevelDto {
    private Integer levelId;
    private String levelName;
    private Date createdTime;
    private Date modifiedTime;
}

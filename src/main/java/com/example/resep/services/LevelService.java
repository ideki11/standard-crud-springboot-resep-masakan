package com.example.resep.services;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.resep.dtos.LevelDto;
import com.example.resep.dtos.ResponseDto;
import com.example.resep.exceptions.NotFoundException;
import com.example.resep.models.Level;
import com.example.resep.repositories.LevelRepository;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class LevelService {
	@Autowired
	LevelRepository levelRepo;

	ModelMapper mapper = new ModelMapper();
	
	public ResponseEntity<Object> readAllLevel() {
		HttpStatus status = HttpStatus.OK;
		
		//get all data level
		List<Level> listAllLevelEntity = levelRepo.findAll();
		
		if (listAllLevelEntity.isEmpty()) {
			throw new NotFoundException("Level", "Data", "Data ist Empty.");
		}else {
			//Konversi dari Entity Ke DTO
			List<LevelDto> listAllLevelDto = new ArrayList<LevelDto>();
			listAllLevelDto = listAllLevelEntity.stream()
					.map(levelEntity -> mapper.map(levelEntity, LevelDto.class))
					.toList();
			
			ResponseDto response = ResponseDto.builder()
										.data(listAllLevelDto)
										.message("Read All Level Success.")
										.status(status.toString())
										.statusCode(status.value())
										.total(listAllLevelDto.size())
										.build();
			return ResponseEntity.status(status).body(response);
		}

	}
}

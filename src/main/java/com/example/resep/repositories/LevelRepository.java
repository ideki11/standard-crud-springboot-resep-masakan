package com.example.resep.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.resep.models.Level;

@Repository
public interface LevelRepository extends JpaRepository<Level, Integer> {
    
}

# Standard CRUD Springboot Resep Masakan

# Guide Step by step membuat CRUD API Springboot

Berikut merupakan guide atau langkah-langkah bagaimana cara membuat Project Springboot secara lengkap.

## Prerequisite
- Java jdk 17.
- Maven 3.6 keatas.
- PostgreSQL 12 Keatas.
- IDE, IntelliJ, VsCode atau Eclipse.
- Pgadmin atau dbeaver.
- Postman / Thunder Client (Vs Code Extenstions).
- Database Resep Masakan.

Siapkan Environment yang dibutuhkan diatas, Jika belum install bisa cek link berikut ini : https://gitlab.com/ideki11/springboot-set-up-environment

## YouTube Overview

[![StandardCRUDAPI](https://img.youtube.com/vi/R2it63AJhHc/0.jpg)](https://www.youtube.com/watch?v=R2it63AJhHc)

# Guide Step by step membuat Project Springboot

Bagi yang belum membuat Struktur Project yang dibutuhkan, silahkan cek link berikut ini : https://gitlab.com/ideki11/structur-project-springboot-resep-masakan

## Step by step CRUD API

## DTO
> Menambahkan DTO class yang dibutuhkan.

1. Pada package atau direktori dtos, tambahkan sebuah DTO baru dengan nama `ErrorDTO`.java untuk kebutuhan standard Template Error yang akan digunakan pada aplikasi. Seperti contoh dibawah ini:
    ```
        import lombok.AllArgsConstructor;
        import lombok.Builder;
        import lombok.Data;
        import lombok.NoArgsConstructor;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        public class ErrorDTO {
            private int statusCode;
            private String message;
            private String details;
        }
    ```
2. Buatlah sebuah DTO baru dengan nama `ResponseDto`.java untuk kebutuhan template Response API, seperti contoh dibawah ini:
    ```
        import lombok.AllArgsConstructor;
        import lombok.Builder;
        import lombok.Data;
        import lombok.NoArgsConstructor;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        public class ResponseDto {
            private long total;
            private Object data;
            private String message;
            private int statusCode;
            private String status;
        }
    ```

## Exceptions Class

> Membuat Exceptions Class

1. Buatlah sebuah Package atau direktori (Folder) baru dengan nama "exceptions".
2. Buatlah sebuah Java class dengan nama `NotFounException`.java. Seperti contoh berikut ini:
    ```
        public class NotFoundException extends RuntimeException {    
            /**
            * 
            */
            private static final long serialVersionUID = -3374972414250960406L;

            public NotFoundException(String resourceName, String fieldName, Object fieldValue) {
                super(String.format("%s not found with %s : '%s'", resourceName, fieldName, fieldValue));

            }
        }
    ```
    - Kodingan di atas adalah sebuah kelas Java yang bernama NotFoundException. Ini adalah kelas custom exception yang digunakan untuk menangani situasi di mana resource tidak ditemukan dalam konteks aplikasi.

    - `public class NotFoundException extends RuntimeException`: Ini adalah deklarasi kelas NotFoundException, yang merupakan turunan dari kelas RuntimeException. Dengan mewarisi kelas RuntimeException, kelas ini adalah sebuah exception yang tidak perlu dideklarasikan dalam blok throws atau ditangkap dalam blok try-catch. Hal ini mempermudah penggunaan dan penanganan pengecualian di dalam kode.

    - `public NotFoundException(String resourceName, String fieldName, Object fieldValue)`: Ini adalah konstruktor dari kelas NotFoundException. Konstruktor ini menerima tiga parameter: resourceName, fieldName, dan fieldValue. Parameter-parameter ini akan digunakan untuk membuat pesan exception yang menjelaskan bahwa resource tidak ditemukan (Not Found) dengan nilai tertentu pada bidang tertentu.

    - Dengan menggunakan kelas NotFoundException, developer dapat dengan mudah menangani kasus-kasus di mana resource not found dalam aplikasi. Exception ini dapat memudahkan untuk memberikan tanggapan yang sesuai kepada pengguna atau sistem lain yang menggunakan aplikasi tersebut.

3. Buatlah sebuah Java class dengan nama `ExceptionHandling`.java, Seperti contoh berikut ini:
    ```
        import org.springframework.http.HttpStatus;
        import org.springframework.web.bind.annotation.ExceptionHandler;
        import org.springframework.web.bind.annotation.ResponseStatus;
        import org.springframework.web.bind.annotation.RestControllerAdvice;

        import com.example.resep.dtos.ErrorDTO;

        @RestControllerAdvice
        public class ExceptionHandling {
            
            @ExceptionHandler(NotFoundException.class)
            @ResponseStatus(HttpStatus.NOT_FOUND)
            public ErrorDTO handleNotFoundException(NotFoundException ex) {
                return new ErrorDTO(
                        HttpStatus.NOT_FOUND.value(), 
                        "application.error.data-not-found.", 
                        ex.getMessage()
                        );		
            }
        }
    ```
    - `@RestControllerAdvice`: Anotasi ini menandai kelas sebagai penasihat global untuk mengelola exception dalam aplikasi Spring MVC yang menghasilkan respons dalam format JSON.

    - `@ExceptionHandler(NotFoundException.class)`: Anotasi ini menandai metode handleNotFoundException() sebagai penangan exception untuk exception NotFoundException. Artinya, metode ini akan dipanggil saat exception NotFoundException terjadi dalam aplikasi.

    - `@ResponseStatus(HttpStatus.NOT_FOUND)`: Anotasi ini menandai bahwa respons HTTP yang dihasilkan oleh metode handleNotFoundException() akan memiliki kode status 404 Not Found.

    - `public ErrorDTO handleNotFoundException(NotFoundException ex)`: Metode ini adalah penangan exception untuk exception NotFoundException. Ini akan menerima objek exception NotFoundException sebagai argumen dan mengembalikan objek ErrorDTO yang berisi informasi kesalahan yang akan dikirim sebagai respons HTTP.

    - `return new ErrorDTO(HttpStatus.NOT_FOUND.value(), "application.error.data-not-found.", ex.getMessage());`: Ini adalah pernyataan yang membuat dan mengembalikan objek ErrorDTO baru dengan kode status 404, pesan kesalahan standar "application.error.data-not-found.", dan pesan spesifik kesalahan yang diambil dari objek pengecualian NotFoundException.

    - Dengan menggunakan kelas ExceptionHandling yang didefinisikan di atas, aplikasi Spring MVC dapat menangani exception NotFoundException dengan menghasilkan respons HTTP yang sesuai dengan kode status 404 dan informasi kesalahan yang relevan dalam format JSON.

## Dependency Modelmapper
> Menambahkan Dependency Modelmapper

1. Buka file pom.xml.
2. Pada didalam atau diantara tag `<dependencies>`, tambahkan dependency Modelmapper seperti contoh berikut ini:
    ```
        <dependencies>
            ...

            <dependency>
                <groupId>org.modelmapper</groupId>
                <artifactId>modelmapper</artifactId>
                <version>3.0.0</version>
            </dependency>

            ...
        </dependencies>
    ```
3. Sehingga pada pom.xml kita sudah menambahkan dependency baru yaitu modelmapper.
    ```
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-data-jpa</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-validation</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-web</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-devtools</artifactId>
                <scope>runtime</scope>
                <optional>true</optional>
            </dependency>
            <dependency>
                <groupId>org.postgresql</groupId>
                <artifactId>postgresql</artifactId>
                <scope>runtime</scope>
            </dependency>
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <optional>true</optional>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-test</artifactId>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.modelmapper</groupId>
                <artifactId>modelmapper</artifactId>
                <version>3.0.0</version>
            </dependency>
        </dependencies>
    ```

## Service Class
> Membuat Service

1. Buatlah sebuah Package atau direktori (Folder) baru dengan nama "services".

2. Buatlah Service dengan nama `LevelService`.java. Untuk Category service hanya berisikan proses GET atau Read data saja, seperti contoh berikut ini:
    ```
        import java.util.ArrayList;
        import java.util.List;

        import org.modelmapper.ModelMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.http.HttpStatus;
        import org.springframework.http.ResponseEntity;
        import org.springframework.stereotype.Service;

        import com.example.resep.dtos.LevelDto;
        import com.example.resep.dtos.ResponseDto;
        import com.example.resep.exceptions.NotFoundException;
        import com.example.resep.models.Level;
        import com.example.resep.repositories.LevelRepository;

        import jakarta.transaction.Transactional;

        @Service
        @Transactional
        public class LevelService {
            @Autowired
            LevelRepository levelRepo;

            ModelMapper mapper = new ModelMapper();
            
            public ResponseEntity<Object> readAllLevel() {
                HttpStatus status = HttpStatus.OK;
                
                //get all data level
                List<Level> listAllLevelEntity = levelRepo.findAll();
                
                if (listAllLevelEntity.isEmpty()) {
                    throw new NotFoundException("Level", "Data", "Data ist Empty.");
                }else {
                    //Konversi dari Entity Ke DTO
                    List<LevelDto> listAllLevelDto = new ArrayList<LevelDto>();
                    listAllLevelDto = listAllLevelEntity.stream()
                            .map(levelEntity -> mapper.map(levelEntity, LevelDto.class))
                            .toList();
                    
                    ResponseDto response = ResponseDto.builder()
                                                .data(listAllLevelDto)
                                                .message("Read All Level Success.")
                                                .status(status.toString())
                                                .statusCode(status.value())
                                                .total(listAllLevelDto.size())
                                                .build();
                    return ResponseEntity.status(status).body(response);
                }

            }
        }
    ```
    - `@Service`: Anotasi ini menandai kelas sebagai sebuah komponen service dalam aplikasi Spring Boot. Kelas yang diberi anotasi @Service biasanya bertanggung jawab untuk menyediakan logika bisnis atau service tertentu dalam aplikasi.

    - `@Transactional`: Anotasi ini menandai bahwa metode-metode dalam kelas akan dijalankan dalam sebuah Transactional. Ini menunjukkan bahwa operasi-operasi dalam metode-metode tersebut akan dianggap sebagai sebuah unit tunggal, dan akan dikembalikan ke keadaan awal jika terjadi kegagalan dalam proses.

    - `@Autowired`: Anotasi ini digunakan untuk memberi tahu Spring untuk melakukan injeksi dependensi pada atribut levelRepo. Dalam hal ini, LevelRepository akan diinjeksikan ke dalam atribut levelRepo, sehingga kelas LevelService dapat mengakses fungsionalitas dari LevelRepository.

    - `ModelMapper mapper = new ModelMapper();`: Ini adalah pembuatan objek dari kelas ModelMapper, yang digunakan untuk melakukan pemetaan (mapping) antara objek dari kelas entitas (Level) ke objek dari kelas DTO (LevelDto). Ini merupakan bagian dari operasi konversi dari entitas ke DTO sebelum mengembalikan respons.

    - `public ResponseEntity<Object> readAllLevel()`: Ini adalah metode yang dibuat pada service, dimana bertanggung jawab untuk membaca semua level yang ada dalam sistem. Metode ini mengembalikan objek `ResponseEntity` yang dapat berisi respons HTTP dengan data level yang berhasil dibaca.
    
    - `HttpStatus status = HttpStatus.OK`;: Ini adalah pembuatan objek HttpStatus dengan nilai OK. Ini akan digunakan sebagai status respons jika operasi berhasil.

    - `List<Level> listAllLevelEntity = levelRepo.findAll();`: Ini adalah pemanggilan metode findAll() dari LevelRepository untuk mendapatkan semua entitas level dari basis data atau database.

    - `if (listAllLevelEntity.isEmpty()) { throw new NotFoundException("Level", "Data", "Data is Empty."); }`: Ini adalah pengecekan apakah daftar entitas level kosong. Jika kosong, maka akan dilemparkan exception NotFoundException dengan pesan yang sesuai.

    - `else { ... }`: Jika daftar entitas level tidak kosong, maka akan dilakukan konversi dari entitas ke DTO menggunakan objek ModelMapper, kemudian sebuah objek ResponseDto akan dibuat dengan data yang sesuai dan dikirim sebagai respons HTTP dengan kode status 200 OK.

    - Dengan menggunakan kelas LevelService seperti di atas, aplikasi Spring Boot dapat menyediakan layanan untuk membaca semua data level dalam sistem. Jika tidak ada data yang ditemukan, maka akan dilemparkan exception NotFoundException.

3. Buatlah Service dengan nama `CategoryService`.java. Untuk Category service hanya berisikan proses GET atau Read data saja, seperti contoh berikut ini:
    ```
        import java.util.ArrayList;
        import java.util.List;

        import org.modelmapper.ModelMapper;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.http.HttpStatus;
        import org.springframework.http.ResponseEntity;
        import org.springframework.stereotype.Service;

        import com.example.resep.dtos.CategoryDto;
        import com.example.resep.dtos.ResponseDto;
        import com.example.resep.exceptions.NotFoundException;
        import com.example.resep.models.Category;
        import com.example.resep.repositories.CategoryRepository;

        import jakarta.transaction.Transactional;

        @Service
        @Transactional
        public class CategoryService {
            @Autowired
            CategoryRepository categoryRepo;

            ModelMapper mapper = new ModelMapper();
            
            public ResponseEntity<Object> readAllCategory() {
                //get all data category
                List<Category> listAllCategory = categoryRepo.findAll();	
                HttpStatus status = HttpStatus.OK;
                
                if (listAllCategory.isEmpty()) {
                    throw new NotFoundException("Category", "Data", "Data is Empty.");
                }else {
                    List<CategoryDto> listAllCategoryDto = new ArrayList<CategoryDto>();
                    listAllCategoryDto = listAllCategory.stream()
                                                        .map(categoryEntity -> mapper.map(categoryEntity, CategoryDto.class))
                                                        .toList();
                    ResponseDto response = ResponseDto.builder()
                                                            .message("Read all Category Succes.")
                                                            .data(listAllCategoryDto)
                                                            .status(status.toString())
                                                            .statusCode(status.value())
                                                            .total(listAllCategoryDto.size())
                                                            .build();
                    
                    return ResponseEntity.status(status).body(response);
                }
                
            }
            
        }
    ```
    - Untuk penjelasan tidak terlalu beda jauh dari LevelService.java, yang berbeda disini disini adalah yang akan kita olah adalah Category.

4. Buatlah Service dengan nama `RecipeService`.java. Untuk Recipe service berisikan proses GET (Read), POST (Create), UPDATE, dan DELETE, seperti contoh berikut ini:
    - Untuk struktur classnya sama seperti pada service-service sebelumnya.
        -   ```
            @Service
            @Transactional
            public class RecipeService {
                @Autowired
                RecipeRepository recipeRepo;
                
                @Autowired
                LevelRepository levelRepo;
                
                @Autowired
                CategoryRepository categoryRepo;
                
                @Autowired
                UsersRepository usersRepo;
                
                ModelMapper mapper = new ModelMapper();
            ```
    - Pertama kita buat dulu method ReadAll Recipe, dimana disini sedikit berbeda karena sudah menggunakan Pagination agar data yang diambil bisa disesuaikan, seperti contoh berikut ini:
        - Disini juga kita harus menambahkan sedikit codingan pada RecipeRepository.java, yaitu menambahkan method untuk digunakan untuk pagination. Seperti contoh berikut ini:

            ```
                @Repository
                public interface RecipeRepository extends JpaRepository<Recipe, Integer>{
                    Page<Recipe> findAll(Pageable pageable);
                }
            ```
            - `Page<Recipe> findAll(Pageable pageable)`: Ini adalah deklarasi metode findAll() yang mengembalikan daftar entitas Recipe. Namun, metode ini menerima sebuah objek Pageable sebagai parameter, yang memungkinkan penggunaan paginasi dalam pengambilan data. Ini berarti bahwa metode ini akan mengembalikan daftar entitas Recipe dalam bentuk halaman (page) sesuai dengan konfigurasi paginasi yang diberikan.

        - Buatlah methode atau logic proses untuk read all Recipe dengan pagination. Sebenarnya hampir sama seperti Read All pada Level dan Category.
        
            ```
                //Read All
                public ResponseEntity<Object> readAllRecipe(int size, int page){
                    HttpStatus status = HttpStatus.OK;
                    
                    //get all data recepi
                    List<Recipe> listAllRecepiEntity = recipeRepo.findAll(PageRequest.of(page, size)).toList();
                    
                    if (listAllRecepiEntity.isEmpty()) {
                        throw new NotFoundException("Recepi", "Data", "Data is Empty");
                    }else {
                        //konversi all entity to DTO
                        List<RecipeDto> listAllRecipeDto = listAllRecepiEntity.stream()
                                                                            .map(entity -> mapper.map(entity, RecipeDto.class))
                                                                            .toList();
                        
                        ResponseDto response = ResponseDto.builder()
                                                                .data(listAllRecipeDto)
                                                                .message("Read All Recipe Success.")
                                                                .status(status.toString())
                                                                .statusCode(status.value())
                                                                .total(listAllRecipeDto.size())
                                                                .build();
                        
                        return ResponseEntity.status(status).body(response);
                    }
                }
            ```
        
    

    - Kedua kita buat read Recipe by Id, seperti berikut ini:
        ```
            //Read by recipe id
            public ResponseEntity<Object> readRecipeById(int recipeId){
                HttpStatus status = HttpStatus.OK;
                
                //get all data recepi
                Recipe recipeEntity = recipeRepo.findById(recipeId).orElseThrow(() -> new NotFoundException("Recipe", "recipe Id", recipeId));
                
                //konversi entity to dto
                RecipeDto recipeDto = mapper.map(recipeEntity, RecipeDto.class);

                ResponseDto response = ResponseDto.builder()
                                                        .data(recipeDto)
                                                        .message("Read Recipe By Id Success.")
                                                        .status(status.toString())
                                                        .statusCode(status.value())
                                                        .total(1)
                                                        .build();
                    
                return ResponseEntity.status(status).body(response);
            }
        ```

        - `public ResponseEntity<Object> readRecipeById(int recipeId)`: Ini adalah deklarasi metode readRecipeById yang menerima satu parameter yaitu recipeId yang merupakan ID resep yang ingin dibaca. Metode ini mengembalikan objek ResponseEntity yang berisi respons HTTP dengan data resep yang berhasil dibaca.
        - `Recipe recipeEntity = recipeRepo.findById(recipeId).orElseThrow(() -> new NotFoundException("Recipe", "recipe Id", recipeId));`: Ini adalah pengambilan entitas resep berdasarkan ID yang diberikan menggunakan metode findById() dari recipeRepo. Jika entitas tidak ditemukan, maka akan dilemparkan pengecualian NotFoundException dengan pesan yang sesuai.
        - `RecipeDto recipeDto = mapper.map(recipeEntity, RecipeDto.class);`: Ini adalah proses konversi dari entitas resep (Recipe) ke DTO resep (RecipeDto) menggunakan objek ModelMapper. Hal ini dilakukan untuk mengubah representasi data dari entitas ke DTO sebelum mengembalikan respons.
        - `ResponseDto response = ResponseDto.builder()...build();`: Ini adalah pembuatan objek ResponseDto yang akan diisi dengan data resep yang berhasil dibaca dan informasi lainnya. Objek ini akan digunakan sebagai respons HTTP yang akan dikirimkan kembali kepada pengguna.
        - `return ResponseEntity.status(status).body(response);`: Ini adalah pernyataan yang mengembalikan objek ResponseEntity yang berisi respons HTTP dengan data resep yang berhasil dibaca. Status respons akan diset sesuai dengan HttpStatus yang telah ditentukan sebelumnya.

    - Ketiga kita buat method untuk logic proses untuk Create Recipe. Seperti contoh berikut ini:
        ```
            //create Recipe
            public ResponseEntity<Object> createRecipe(RecipeDto body){
                //validasi data
                Level levelEntity = levelRepo.findById(body.getLevel().getLevelId()).orElseThrow(() -> new NotFoundException("Level", "level Id", body.getLevel().getLevelId()));
                Category categoryEntity = categoryRepo.findById(body.getCategory().getCategoryId()).orElseThrow(() -> new NotFoundException("Category", "category Id", body.getCategory().getCategoryId()));
                Users userEntity = usersRepo.findById(body.getUsers().getUserId()).orElseThrow(()-> new NotFoundException("Users", "user id", body.getUsers().getUserId()));
                
                //create Recipe entity, konversi ke entity dengan modelmapper
                Recipe recipeEntity = mapper.map(body, Recipe.class);
                                    
                //save data recipe
                recipeRepo.save(recipeEntity);
                
                String responseMessage = "application.success.create.recipe";
                HttpStatus status = HttpStatus.CREATED;
                
                return ResponseEntity.status(status).body(responseMessage);
            }
        ```
        - `Validasi Data`: Pertama-tama, dilakukan validasi data untuk memastikan bahwa entitas level, kategori, dan pengguna yang terkait dengan resep yang akan dibuat tersedia dalam basis data. Ini dilakukan dengan cara:
            * Mencari entitas level berdasarkan ID level yang terdapat dalam objek RecipeDto.
            * Mencari entitas kategori berdasarkan ID kategori yang terdapat dalam objek RecipeDto.
            * Mencari entitas pengguna (user) berdasarkan ID pengguna yang terdapat dalam objek RecipeDto.
        
        - `Pembuatan Entitas Resep`: Setelah validasi data selesai, langkah berikutnya adalah membuat entitas resep baru. Ini dilakukan dengan cara mengonversi objek `RecipeDto` menjadi objek entitas `Recipe` menggunakan objek `ModelMapper`.

        - `Penyimpanan Data Resep`: Setelah entitas resep baru berhasil dibuat, langkah selanjutnya adalah menyimpannya ke dalam basis data. Ini dilakukan dengan menggunakan metode `save()` dari repositori recipeRepo.

        - `Pembentukan Respons`: Setelah penyimpanan data berhasil dilakukan, respons HTTP yang akan dikirimkan kembali kepada pengguna dibentuk. Respons ini berisi pesan sukses yang menandakan bahwa data resep telah berhasil dibuat, serta status respons 201 Created.

        - Dengan menggunakan metode createRecipe seperti di atas, kelas service ini memungkinkan untuk membuat data resep baru berdasarkan data yang diberikan dalam objek RecipeDto dan mengembalikan respons HTTP yang sesuai dengan hasil pembuatan tersebut.
    
    - Keempat disini kita akan membuat method update atau logic proses untuk edit Recipe. Seperti contoh berikut ini:
        ```
            //update recipe
            public ResponseEntity<Object> editRecipe(int recipeId, RecipeDto body){
                //validasi data
                Recipe recipe = recipeRepo.findById(recipeId).orElseThrow(()-> new NotFoundException("Recipe", "recipe id", recipeId));

                Level levelEntity = levelRepo.findById(body.getLevel().getLevelId()).orElseThrow(() -> new NotFoundException("Level", "level Id", body.getLevel().getLevelId()));
                Category categoryEntity = categoryRepo.findById(body.getCategory().getCategoryId()).orElseThrow(() -> new NotFoundException("Category", "category Id", body.getCategory().getCategoryId()));
                Users userEntity = usersRepo.findById(body.getUsers().getUserId()).orElseThrow(()-> new NotFoundException("Users", "user id", body.getUsers().getUserId()));
                
                recipe.setCategory(categoryEntity);
                recipe.setLevel(levelEntity);
                recipe.setUsers(userEntity);
                recipe.setRecipeName(body.getRecipeName());
                recipe.setHowToCook(body.getHowToCook());
                recipe.setImageFilename(body.getImageFilename());
                recipe.setIngredient(body.getIngredient());
                recipe.setTimeCook(body.getTimeCook());
                                    
                //save data recipe
                recipeRepo.save(recipe);
                
                String responseMessage = "application.success.update.recipe";
                HttpStatus status = HttpStatus.OK;
                
                return ResponseEntity.status(status).body(responseMessage);
            }
        ```
        - Kodingan di atas adalah bagian dari sebuah metode dalam sebuah kelas layanan (service) yang bertugas untuk mengubah data resep berdasarkan ID resep yang diberikan dan data baru yang diberikan dalam objek RecipeDto.

        - `Validasi Data`: Pertama-tama, dilakukan validasi data untuk memastikan bahwa resep yang akan diubah memang tersedia dalam basis data. Ini dilakukan dengan mencari entitas resep berdasarkan ID resep yang diberikan. Jika resep tidak ditemukan, maka dilemparkan pengecualian NotFoundException.

        - `Validasi Data Terkait`: Setelah itu, dilakukan validasi data terkait, yaitu:
           * Mencari entitas level berdasarkan ID level yang terdapat dalam objek RecipeDto.
           * Mencari entitas kategori berdasarkan ID kategori yang terdapat dalam objek RecipeDto.
           * Mencari entitas pengguna (user) berdasarkan ID pengguna yang terdapat dalam objek RecipeDto.
        
        - `Pengubahan Data Resep atau Edit Data Resep`: Setelah validasi data selesai, langkah berikutnya adalah mengubah data resep berdasarkan data yang baru diberikan dalam objek RecipeDto. Data yang diubah meliputi:
            * Kategori (categoryEntity)
            * Level (levelEntity)
            * Pengguna (userEntity)
            * Nama resep (recipeName)
            * Cara memasak (howToCook)
            * Nama file gambar (imageFilename)
            * Bahan (ingredient)
            * Waktu memasak (timeCook)
        
        - `Penyimpanan Data Resep`: Setelah pengubahan data resep berhasil dilakukan, data resep yang sudah diubah disimpan ke dalam basis data menggunakan metode `save()` dari repositori `recipeRepo`.

        - `Pembentukan Respons`: Setelah penyimpanan data berhasil dilakukan, respons HTTP yang akan dikirimkan kembali kepada pengguna dibentuk. Respons ini berisi pesan sukses yang menandakan bahwa data resep telah berhasil diubah, serta status respons 200 OK.

        - Dengan menggunakan metode editRecipe seperti di atas, kelas service ini memungkinkan untuk mengubah data resep berdasarkan ID resep yang diberikan dan data baru yang diberikan dalam objek RecipeDto, dan mengembalikan respons HTTP yang sesuai dengan hasil pengubahan tersebut.
    
    - Kelima kita akan membuat method atau logic proses untuk Delete Recipe berdasarkan ID Recipe. Seperti Contoh Berikut ini.
        ```
            //detele recipe
            public ResponseEntity<Object> deleteRecipe(int recipeId){
                //validasi Recipe
                Recipe recipe = recipeRepo.findById(recipeId).orElseThrow(()-> new NotFoundException("Recipe", "recipe id", recipeId));
                
                //delete
                recipeRepo.delete(recipe);
                
                String responseMessage = "application.success.delete.recipe";
                HttpStatus status = HttpStatus.OK;
                
                return ResponseEntity.status(status).body(responseMessage);
            }
        ```
        - Kodingan di atas adalah bagian dari sebuah metode dalam sebuah kelas layanan (service) yang bertugas untuk menghapus data resep berdasarkan ID resep yang diberikan.

        - `Pengecekan Ketersediaan Resep`: Pertama-tama, dilakukan pengecekan apakah resep dengan ID yang diberikan tersedia dalam data base. Ini dilakukan dengan mencari entitas resep berdasarkan ID resep yang diberikan. Jika resep tidak ditemukan, maka dilemparkan pengecualian NotFoundException.

        - `Penghapusan Resep`: Setelah memastikan bahwa resep tersedia, langkah berikutnya adalah menghapus resep tersebut dari database. Ini dilakukan dengan menggunakan metode `delete()` dari repositori `recipeRepo`, dengan menyertakan objek resep yang akan dihapus.

        - `Pembentukan Respons`: Setelah penghapusan resep berhasil dilakukan, respons HTTP yang akan dikirimkan kembali kepada pengguna dibentuk. Respons ini berisi pesan sukses yang menandakan bahwa data resep telah berhasil dihapus, serta status respons 200 OK.

        - Dengan menggunakan metode deleteRecipe seperti di atas, kelas service ini memungkinkan untuk menghapus data resep berdasarkan ID resep yang diberikan, dan mengembalikan respons HTTP yang sesuai dengan hasil penghapusan tersebut.

## Controller Class
> Membuat Controller

1. Buatlah sebuah Package atau direktori (Folder) baru dengan nama "controllers".
2. Buatlah sebuah controller dengan nama LevelController.java, kemudian tambahkan kodingan seperti contoh berikut ini:
    ```
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.http.ResponseEntity;
        import org.springframework.web.bind.annotation.GetMapping;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RestController;

        import com.example.resep.services.LevelService;

        @RestController
        @RequestMapping("/api/level")
        public class LevelController {
            @Autowired
            LevelService levelService;
            
            //API read all Level
            @GetMapping("/read-all-level")
            public ResponseEntity<Object> readAllLevel(){
                return levelService.readAllLevel();
            }
        }
    ```
    - Kodingan di atas adalah bagian dari sebuah kelas controller Spring Boot yang bertanggung jawab untuk menangani permintaan terkait level resep.
    - `Anotasi @RestController`: Kelas LevelController diberi `anotasi @RestController`, yang menandakan bahwa kelas ini merupakan sebuah controller yang akan menangani permintaan web dan menghasilkan respons dalam format JSON. Ini berarti setiap metode dalam kelas ini akan secara otomatis mengembalikan objek yang akan diubah menjadi JSON dan dikirimkan kembali kepada klien.

    - `Anotasi @RequestMapping`: Anotasi ini menetapkan jalur dasar URL untuk semua metode dalam kelas. Dalam hal ini, setiap metode dalam LevelController akan memiliki jalur URL awal `/api/level`.

    - Anotasi @Autowired. Ini memungkinkan kita untuk menggunakan objek dari kelas LevelService tanpa harus membuat instansinya secara eksplisit.

    - `Metode API readAllLevel`: Metode ini diberi anotasi `@GetMapping` dan memiliki jalur URL tambahan `/read-all-level`. Ini berarti metode ini akan menangani permintaan HTTP GET yang datang ke `/api/level/read-all-level`. Metode ini akan memanggil metode `readAllLevel()` dari kelas LevelService untuk membaca semua level yang ada dalam sistem. Kemudian, respons yang diterima dari layanan tersebut akan langsung dikembalikan (response) kepada klien.

    - Dengan menggunakan kelas LevelController seperti di atas, kita memiliki sebuah endpoint API yang memungkinkan klien untuk membaca semua level resep dalam sistem dengan melakukan permintaan HTTP `GET` ke `/api/level/read-all-level`. Metode ini memanfaatkan layanan LevelService untuk menjalankan logika bisnis yang sesuai.

3. Buatlah sebuah controller dengan nama CategoryController.java, kemudian tambahkan kodingan seperti contoh berikut ini:
    ```
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.http.ResponseEntity;
        import org.springframework.web.bind.annotation.GetMapping;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RestController;

        import com.example.resep.services.CategoryService;

        @RestController
        @RequestMapping("/api/category")
        public class CategoryController {
            @Autowired
            CategoryService categoryService;
            
            //API Read all Category
            @GetMapping("/read-all-category")
            public ResponseEntity<Object> readAllCategory(){
                return categoryService.readAllCategory();
            }
        }
    ```
    - Untuk penjelasan kodingan hampir sama dengan yang ada pada Level Service.

4. Buatlah sebuah controller dengan nama RecipeController.java, Pada RecipeController.java mungkin sedikit berbeda daripada class controller yang ada pada Level dan Category. Dikarenkan Pada RecipeController terdapat CRUD API, sesuai dengan yang ada pada RecipeService.java. Seperti contoh berikut ini:
    - Untuk struktur Controller masih sama seperti pada class controller sebelumnya.
        ```
            import org.springframework.beans.factory.annotation.Autowired;
            import org.springframework.http.ResponseEntity;
            import org.springframework.web.bind.annotation.DeleteMapping;
            import org.springframework.web.bind.annotation.GetMapping;
            import org.springframework.web.bind.annotation.PathVariable;
            import org.springframework.web.bind.annotation.PostMapping;
            import org.springframework.web.bind.annotation.PutMapping;
            import org.springframework.web.bind.annotation.RequestBody;
            import org.springframework.web.bind.annotation.RequestMapping;
            import org.springframework.web.bind.annotation.RequestParam;
            import org.springframework.web.bind.annotation.RestController;

            import com.example.resep.dtos.RecipeDto;
            import com.example.resep.services.RecipeService;

            @RestController
            @RequestMapping("/api/recipe")
            public class RecipeController {
                @Autowired
                RecipeService recipeService;
                
                //read all recipe
               
                
                //read recipe by id
               
                
                //create recipe
                
                
                //update recipe
                
                
                //delete recipe
               
            }
        ```

    - Pertama buat Method API untuk ReadAllRecipe, seperti contoh berikut ini:
        ```
            //read all recipe
            @GetMapping("/read-all-recipe")
            public ResponseEntity<Object> getAllRecipe(@RequestParam(name = "size", defaultValue = "10")int size, @RequestParam(name = "page", defaultValue = "0") int page){
                return recipeService.readAllRecipe(size, page);
            }
        ```
        - `Anotasi @GetMapping`: Metode getAllRecipe diberi `anotasi @GetMapping`, yang menandakan bahwa metode ini akan menangani permintaan HTTP GET.
        - `Anotasi @RequestParam`: Di dalam parameter metode getAllRecipe, terdapat `anotasi @RequestParam`. Anotasi ini digunakan untuk mendapatkan nilai dari parameter yang dikirimkan melalui URL. Dalam contoh ini, terdapat dua parameter:
            * `size`: Parameter ini digunakan untuk menentukan jumlah item (size) per halaman ketika melakukan paginasi. Jika tidak ada nilai yang diberikan, `nilai defaultnya adalah 10`.
            * `page`: Parameter ini digunakan untuk menentukan halaman yang ingin ditampilkan ketika melakukan paginasi. Jika tidak ada nilai yang diberikan, `nilai defaultnya adalah 0`.
        - `Pemanggilan Metode service`: Metode getAllRecipe kemudian memanggil metode readAllRecipe dari service terkait (dalam hal ini, recipeService). Metode ini menerima dua parameter, yaitu size dan page, yang nilainya diperoleh dari parameter yang diberikan dalam permintaan HTTP.
        - `Respons`: Hasil dari metode readAllRecipe akan berupa respons HTTP yang berisi daftar resep sesuai dengan ukuran dan halaman yang ditentukan. Respons ini dikembalikan kepada klien sebagai objek ResponseEntity.

    - Kedua buat Method API untuk Read Recipe By Id, seperti contoh berikut ini:
        ```
            //read recipe by id
            @GetMapping("/read-all-recipe/{recipeId}")
            public ResponseEntity<Object> readAllRecipeById(@PathVariable(name = "recipeId") int recipeId){
                return recipeService.readRecipeById(recipeId);
            }                                                           
        ```
        - `Anotasi @GetMapping`: Metode readAllRecipeById diberi `anotasi @GetMapping`, yang menandakan bahwa metode ini akan menangani permintaan HTTP GET.

        - `Path Variable`: Di dalam jalur URL yang ditentukan `("/read-all-recipe/{recipeId}")`, terdapat placeholder {recipeId} yang menandakan bahwa nilai ID resep akan ditentukan oleh klien sebagai bagian dari URL. A`notasi @PathVariable` digunakan untuk menangkap nilai ID resep tersebut dan menyimpannya dalam parameter recipeId metode.

        - `Pemanggilan Metode Service`: Setelah mendapatkan ID resep dari jalur URL, metode readAllRecipeById kemudian memanggil metode readRecipeById dari service terkait (dalam hal ini, recipeService). Metode ini menerima parameter recipeId yang merupakan ID resep yang diminta.

        - `Respons`: Metode readRecipeById dari layanan recipeService akan mengembalikan respons HTTP yang berisi detail resep yang diminta berdasarkan ID-nya. Respons ini kemudian dikembalikan kepada klien sebagai objek ResponseEntity.

        - Dengan menggunakan metode `readAllRecipeById` seperti di atas, kelas controller ini memungkinkan klien untuk membaca detail resep tertentu dalam sistem dengan melakukan permintaan HTTP GET ke `/read-all-recipe/{recipeId}`, di mana `{recipeId}` adalah ID resep yang diinginkan.

    - Ketiga buat Method API untuk Create Recipe, seperti contoh berikut ini:
        ```
            //create recipe
            @PostMapping("/create-recipe")
            public ResponseEntity<Object> createRecipe(@RequestBody RecipeDto body){
                return recipeService.createRecipe(body);
            }
        ```
        - `Anotasi @PostMapping`: Metode createRecipe diberi `anotasi @PostMapping`, yang menandakan bahwa metode ini akan menangani permintaan HTTP POST.
        
        - `Path`: Metode ini menetapkan jalur URL `/create-recipe` sebagai titik akhir API untuk membuat resep baru. Ketika klien melakukan permintaan HTTP POST ke jalur ini, controller akan menangani permintaan tersebut.

        - `Parameter @RequestBody`: Metode ini memiliki parameter `@RequestBody RecipeDto body`. Anotasi @RequestBody digunakan untuk mengikat data yang dikirimkan dalam badan permintaan HTTP ke objek RecipeDto. Dengan demikian, data resep yang dikirimkan oleh klien dalam format JSON akan diubah menjadi objek RecipeDto oleh Spring.

        - `Pemanggilan Metode Service`: Setelah mendapatkan data resep dalam bentuk objek RecipeDto, metode createRecipe kemudian memanggil metode createRecipe dari service terkait (recipeService). Data resep yang dikirimkan sebagai argumen.

        - `Respons`: Metode createRecipe dari layanan recipeService akan mengembalikan respons HTTP yang berisi hasil dari operasi pembuatan resep baru. Respons ini kemudian dikembalikan kepada klien sebagai objek ResponseEntity.

        - Dengan menggunakan metode `createRecipe` seperti di atas, kelas controller ini memungkinkan klien untuk membuat resep baru dalam sistem dengan melakukan permintaan HTTP POST ke `/create-recipe`, dengan data resep yang dikirimkan dalam request body.
        
    - Keempat membuat Method API untuk Update / Edit Recipe, seperti contoh berikut ini:
        ```
            //update recipe
            @PutMapping("/edit-recipe/{recipeId}")
            public ResponseEntity<Object> updateRecipe(@PathVariable(name = "recipeId") int recipeId, @RequestBody RecipeDto body){
                return recipeService.editRecipe(recipeId, body);
            }
        ```
        - `Anotasi @PutMapping`: Metode updateRecipe diberi anotasi `@PutMapping`, yang menandakan bahwa metode ini akan menangani permintaan HTTP PUT. Ini berarti metode ini digunakan untuk memperbarui data resep yang sudah ada.

        - `Path`: Metode ini menetapkan jalur URL `/edit-recipe/{recipeId}` sebagai titik akhir API untuk memperbarui resep. `{recipeId}` adalah path variable yang menunjukkan ID resep yang akan diperbarui. Ketika klien melakukan permintaan HTTP PUT ke jalur ini dengan menyertakan ID resep yang spesifik, controller akan menangani permintaan tersebut.

        - `Parameter @PathVariable`: Metode ini memiliki parameter `@PathVariable(name = "recipeId") int recipeId`, yang digunakan untuk menangkap nilai ID resep yang diberikan dalam jalur URL. Nilai ID resep ini kemudian digunakan untuk menentukan resep mana yang akan diperbarui.

        - `Parameter @RequestBody`: Metode ini juga memiliki parameter `@RequestBody RecipeDto body`. Anotasi `@RequestBody` digunakan untuk mengikat data yang dikirimkan dalam badan permintaan HTTP ke objek RecipeDto. Dengan demikian, data resep yang dikirimkan oleh klien dalam format JSON akan diubah menjadi objek RecipeDto oleh Spring.

        - `Pemanggilan Metode Service`: Setelah mendapatkan ID resep dan data resep yang baru dalam bentuk objek RecipeDto, metode updateRecipe kemudian memanggil metode editRecipe dari service terkait (recipeService). Metode ini menerima dua parameter, yaitu ID resep yang akan diperbarui dan data resep baru.

        - `Respons`: Metode editRecipe dari layanan recipeService akan mengembalikan respons HTTP yang berisi hasil dari operasi pembaruan resep. Respons ini kemudian dikembalikan kepada klien sebagai objek ResponseEntity.

        - Dengan menggunakan metode `updateRecipe` seperti di atas, kelas controller ini memungkinkan klien untuk memperbarui data resep dalam sistem dengan melakukan permintaan HTTP PUT ke `/edit-recipe/{recipeId}`, dengan data resep yang baru dikirimkan dalam Request Body.
    
    - Kelima membuat Method API untuk Delete Recipe, seperti contoh berikut ini:
        ```
            //delete recipe
            @DeleteMapping("/delete-recipe/{recipeId}")
            public ResponseEntity<Object> deleteRecipe(@PathVariable(name = "recipeId") int recipeId){
                return recipeService.deleteRecipe(recipeId);
            }
        ```
        - `Anotasi @DeleteMapping`: Metode deleteRecipe diberi anotasi `@DeleteMapping`, yang menandakan bahwa metode ini akan menangani permintaan HTTP DELETE. Ini berarti metode ini digunakan untuk menghapus data resep yang sudah ada.

        - `Path`: Metode ini menetapkan jalur URL `/delete-recipe/{recipeId}` sebagai titik akhir atau endpoint API untuk menghapus resep. `{recipeId}` adalah path variable yang menunjukkan ID resep yang akan dihapus. Ketika klien melakukan permintaan HTTP DELETE ke jalur ini dengan menyertakan ID resep yang spesifik, controller akan menangani permintaan tersebut.

        - `Parameter @PathVariable`: Metode ini memiliki parameter `@PathVariable(name = "recipeId") int recipeId`, yang digunakan untuk menangkap nilai ID resep yang diberikan dalam jalur URL. Nilai ID resep ini kemudian digunakan untuk menentukan resep mana yang akan dihapus.

        - `Pemanggilan Metode Service`: Setelah mendapatkan ID resep yang akan dihapus, metode deleteRecipe kemudian memanggil metode deleteRecipe dari service terkait (recipeService). Metode ini menerima satu parameter, yaitu ID resep yang akan dihapus.

        - `Respons`: Metode deleteRecipe dari recipeService akan mengembalikan respons HTTP yang berisi hasil dari operasi penghapusan resep. Respons ini kemudian dikembalikan kepada klien sebagai objek ResponseEntity.

        - Dengan menggunakan metode deleteRecipe seperti di atas, kelas controller ini memungkinkan klien untuk menghapus data resep dalam sistem dengan melakukan permintaan HTTP DELETE ke `/delete-recipe/{recipeId}`, di mana `{recipeId}` adalah ID resep yang akan dihapus.

## Postman
> Mencoba API yang sudah dibuat di Postman atau thunder client.

1. Buka aplikasi Postman atau thunder client.
2. API Read All Recipe. Untuk menggunakan API tersebut kita hanya tinggal masukan URL dari API yang ingin kita gunakan pada postman atau thunder client, seperti berikut ini:
    ![ReadAllPostman][def]

    [def]: img/ReadAllPostman.PNG

    - Permintaan tersebut menggunakan metode GET untuk mengambil data resep dari API yang dijalankan pada localhost dengan port 8080
    - `GET` : merupakan method yang digunakan pada API Read All Recipe yang kita buat pada controller.
    - `Protokol dan Host`: `localhost:8080` menunjukkan bahwa API diakses melalui protokol HTTP pada mesin lokal atau localhost dengan port 8080. Ketika menggunakan Postman, ini akan mengarah ke server yang berjalan pada komputer lokal Anda.
    - `Path Endpoint`: `/api/recipe/read-all-recipe` adalah jalur (path) endpoint API yang digunakan untuk membaca semua resep. Ini menunjukkan bahwa permintaan ini ditujukan untuk mengakses operasi "Read All Recipe" pada API.
    - `Query Parameters`: `?size=5&page=0` adalah parameter query yang digunakan untuk menentukan ukuran (size) halaman dan nomor halaman yang ingin diambil dari hasil resep. Dalam kasus ini:
        * `size=5`: Parameter ini menentukan jumlah maksimum resep yang akan ditampilkan dalam satu halaman. Dalam contoh ini, hanya lima resep yang akan ditampilkan dalam satu halaman.
        * `page=0`: Parameter ini menentukan nomor halaman yang ingin diambil. Dalam contoh ini, halaman yang diminta adalah halaman pertama, yang dimulai dari indeks 0.
    - Ketika permintaan ini dikirim menggunakan Postman dengan metode GET, API akan memprosesnya dan mengembalikan lima resep pertama dari basis data, dengan format dan struktur yang telah ditentukan oleh logika backend. Postman kemudian akan menampilkan respons dari API sesuai dengan hasil yang diberikan.

2. API Read Recipe By Id, Untuk menggunakan API tersebut kita hanya tinggal masukan URL dari API yang ingin kita gunakan pada postman atau thunder client, seperti berikut ini:
    ![ReadByID][def1]

    [def1]: img/ReadByIdPostman.PNG

    - Permintaan tersebut menggunakan metode GET untuk mengambil data resep dengan ID tertentu dari API yang dijalankan pada localhost dengan port 8080.
    - `Protokol dan Host`: `localhost:8080` menunjukkan bahwa API diakses melalui protokol HTTP pada mesin lokal (localhost) dengan port 8080. Ketika menggunakan Postman, ini akan mengarah ke server yang berjalan pada komputer lokal Anda.
    - `Path Endpoint`: `/api/recipe/read-all-recipe/2` adalah jalur (path) endpoint API yang digunakan untuk membaca resep dengan ID tertentu. Angka 2 diakhir jalur menunjukkan ID resep yang ingin diambil. Ini menunjukkan bahwa permintaan ini ditujukan untuk mengakses operasi "Read Recipe dengan ID 2" pada API.
    - Ketika permintaan ini dikirim menggunakan Postman dengan metode GET, API akan memprosesnya dan mengembalikan data resep yang memiliki ID 2 dari database. Postman kemudian akan menampilkan respons dari API sesuai dengan hasil yang diberikan.

3. API Create Recipe, Untuk menggunakan API tersebut kita hanya tinggal masukan URL dari API yang ingin kita gunakan pada postman atau thunder client, seperti berikut ini:
    ![CreateRecipe][def2]

    [def2]: img/CreateRecipe.PNG

    - Permintaan tersebut menggunakan metode POST untuk membuat resep baru dalam API yang dijalankan pada localhost dengan port 8080.
    - `Protokol dan Host`: `localhost:8080` menunjukkan bahwa API diakses melalui protokol HTTP pada mesin lokal atau localhost dengan port 8080.
    - `Path Endpoint`: `/api/recipe/create-recipe` adalah jalur (path) endpoint API yang digunakan untuk membuat resep baru. Ini menunjukkan bahwa permintaan ini ditujukan untuk mengakses operasi "Create New Recipe" pada API.
    - `Metode HTTP`: `Metode POST` menunjukkan bahwa data yang dikirimkan dalam body request akan digunakan untuk membuat entitas baru di server.
    - `Request Body`: `Request Body` berisi data resep baru yang akan dibuat. Dalam kasus ini, Request Body adalah JSON yang berisi detail resep baru, termasuk nama resep, cara memasak, nama gambar, bahan-bahan, waktu memasak, kategori, tingkat kesulitan, dan ID pengguna yang membuat resep. Data ini akan digunakan oleh API untuk membuat resep baru dalam sistem. Berikut adalah contoh isi dari Request Body untuk Recipe:
        ```
            {
                "recipeName": "Bubur Ayam Mang Radili",
                "howToCook": "1. Masak nasi; 2. Tunggu beberapa menit",
                "imageFilename": "https://asset.kompas.com/crops/Kyp-MBp3Kf0PLGveth_zzhU2gfI=/0x0:1000x667/750x500/data/photo/2020/07/11/5f09e008e7fee.jpg",
                "ingredient": "Nasi; Air; Daun salam; bawang",
                "timeCook": 60,
                "category": {
                    "categoryId" : 2
                },
                "level": {
                    "levelId" : 4
                },
                "users" : {
                    "userId" : 1
                }
            }
        ```
    - Ketika permintaan ini dikirim menggunakan Postman dengan metode POST dan Request Body yang diberikan, API akan memprosesnya dengan membuat resep baru sesuai dengan data yang diberikan dalam Request Body. Setelah itu, API akan mengembalikan respons yang sesuai, mungkin berupa konfirmasi pembuatan resep baru atau pesan kesalahan jika ada masalah dalam proses pembuatan resep. Postman kemudian akan menampilkan respons dari API sesuai dengan hasil yang diberikan.

4. API Update Recipe, Untuk menggunakan API tersebut kita hanya tinggal masukan URL dari API yang ingin kita gunakan pada postman atau thunder client, seperti berikut ini:
    ![UpdateRecipe][def3]

    [def3]: img/UpdateRecipe.PNG

    - Permintaan tersebut menggunakan metode PUT untuk mengedit resep yang memiliki ID 10 dalam API yang dijalankan pada localhost dengan port 8080.
    - `Path Endpoint`: `/api/recipe/edit-recipe/10` adalah jalur (path) endpoint API yang digunakan untuk mengedit resep dengan ID `10`. Ini menunjukkan bahwa permintaan ini ditujukan untuk mengakses operasi "Edit atau Update Recipe dengan ID 10" pada API.
    - `Metode HTTP`: Metode `PUT` menunjukkan bahwa data yang dikirimkan dalam Request Body akan digunakan untuk mengedit entitas yang sudah ada di server.
    - `Request Body`: Request Body berisi data yang akan digunakan untuk mengedit resep yang memiliki ID 10. Dalam kasus ini, Request Body adalah JSON yang berisi detail resep yang akan diedit, termasuk nama resep yang baru, cara memasak yang baru, gambar yang baru, bahan-bahan yang baru, waktu memasak yang baru, kategori yang baru, tingkat kesulitan yang baru. Data ini akan digunakan oleh API untuk mengedit resep yang sesuai dalam sistem.
    - Ketika permintaan ini dikirim menggunakan Postman dengan metode PUT dan Request Body yang diberikan, API akan memprosesnya dengan mengedit resep yang memiliki ID 10 sesuai dengan data yang diberikan dalam Request Body. Setelah itu, API akan mengembalikan respons yang sesuai, mungkin berupa konfirmasi pengeditan resep atau pesan kesalahan jika ada masalah dalam proses pengeditan. Postman kemudian akan menampilkan respons dari API sesuai dengan hasil yang diberikan.

5. API Update Delete, Untuk menggunakan API tersebut kita hanya tinggal masukan URL dari API yang ingin kita gunakan pada postman atau thunder client, seperti berikut ini:
    ![DeleteRecipe][def4]

    [def4]: img/DeletePostman.PNG

    - Permintaan ini adalah contoh penggunaan metode DELETE untuk menghapus entitas resep dengan ID 10 dari API yang di-host di localhost dengan port 8080.
    - `Protokol dan Host`: `localhost:8080` menunjukkan bahwa kita mengakses API melalui protokol HTTP pada localhost dengan port 8080.
    - `Path Endpoint`: `/api/recipe/delete-recipe/10` adalah jalur (path) endpoint API yang digunakan untuk menghapus resep dengan ID `10`. Ini menunjukkan bahwa permintaan ini bertujuan untuk mengakses operasi "Delete Recipe dengan ID 10" pada API.
    - `Metode HTTP`: Permintaan ini menggunakan metode `DELETE`, yang menandakan bahwa kita ingin menghapus data dari server. Dalam konteks ini, kita ingin menghapus entitas resep dengan ID 10.
    - Ketika permintaan ini dikirim menggunakan Postman dengan metode `DELETE`, API akan memprosesnya dengan menghapus resep yang memiliki ID 10 dari database. Setelah itu, API akan mengembalikan respons yang sesuai, mungkin berupa konfirmasi penghapusan resep atau pesan kesalahan jika ada masalah dalam proses penghapusan. Postman akan menampilkan respons dari API sesuai dengan hasil yang diberikan.